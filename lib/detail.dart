import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  String id;
  Detail({this.id});

  @override
  _DetailState createState() => _DetailState(id: id);
}

class _DetailState extends State<Detail> {
  String id;
  _DetailState({this.id});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: Container(
         child: Center(child: Text('$id'),),
      ),
    );
  }
}