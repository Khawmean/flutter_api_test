import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

class TestApp extends StatefulWidget {
  TestApp({Key key}) : super(key: key);

  @override
  _TestAppState createState() => _TestAppState();
}

class _TestAppState extends State<TestApp> {
  List list = new List();
  @override
  Widget build(BuildContext context) {
    list.add({"name": "Coco", "date": 12});
    list.add({"name": "Vichika", "date": 12});
    list.add({"name": "Vona", "date": 12});
    list.add({"name": "Seyha", "date": 12});
    list.add({"name": "Heang", "date": 16});
    list.add({"name": "Vona", "date": 16});
    list.add({"name": "Khanha", "date": 17});
    list.add({"name": "Vuthika", "date": 17});
    list.add({"name": "Vona", "date": 17});
    list.add({"name": "Sirika", "date": 13});
    list.add({"name": "Heang", "date": 13});
    list.add({"name": "Bunna", "date": 13});
    list.add({"name": "kaka", "date": 12});
    list.add({"name": "Heang", "date": 12});
    list.add({"name": "Nova", "date": 13});
    list.add({"name": "Chiva", "date": 13});
    list.add({"name": "Coco", "date": 12});
    list.add({"name": "Vichika", "date": 12});
    list.add({"name": "Vona", "date": 12});
    list.add({"name": "Seyha", "date": 12});
    list.add({"name": "Heang", "date": 16});
    list.add({"name": "Vona", "date": 16});
    list.add({"name": "Khanha", "date": 17});
    list.add({"name": "Vuthika", "date": 17});
    list.add({"name": "Vona", "date": 17});
    list.add({"name": "Sirika", "date": 13});
    list.add({"name": "Heang", "date": 13});
    list.add({"name": "Bunna", "date": 13});
    list.add({"name": "kaka", "date": 12});
    list.add({"name": "Heang", "date": 12});
    list.add({"name": "Nova", "date": 13});
    list.add({"name": "Chiva", "date": 13});
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Testing App'),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text('This is BBB'),
                Container(
                  height: 700,
                  child: buildData(list),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

buildData(List lst) {
  List nList = List();
  List sList = List();
  int current = 0;
  nList.clear();
  for (var v in lst) {
    if (current == v['date']) {
      sList.add({"name": v['name'], "date": v['date']});
    } else {
      current = v['date'];
      if (sList.isNotEmpty) {
        nList.add({"head": sList[0]['date'], "data": sList.toList()});
        sList.clear();
      }
      sList.add({"name": v['name'], "date": v['date']});
    }
  }
  nList.add({"head": sList[0]['date'], "data": sList.toList()});
  sList.clear();

  for (var item in nList) {
    print('Not null >>> $item');
  }

  return ListView.builder(
      itemCount: nList.length,
      itemBuilder: (context, sn) {
        return StickyHeader(
          header: Container(
            height: 30.0,
            color: Colors.blueGrey[700],
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            alignment: Alignment.centerLeft,
            child: Text(
              '${nList[sn]['head']}',
              style: const TextStyle(color: Colors.white),
            ),
          ),
          content: SingleChildScrollView(
            physics: ScrollPhysics(),
            child: Column(
              children: <Widget>[
                ListView.builder(
                    itemCount: nList[sn]['data'].length,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, vl) {
                      return ListTile(
                        onTap: () {},
                        title: Text('${nList[sn]['data'][vl]['name']}'),
                        trailing: Text('${nList[sn]['data'][vl]['date']}'),
                      );
                    })
              ],
            ),
          ),
        );
      });
}

// itmBuild(nList) {
//   return
// }
