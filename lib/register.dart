import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:test_flutter/student.dart';

class Register extends StatefulWidget {
  Data student = Data();
  bool update = false;
  Register({Key key, this.student, this.update}) : super(key: key);

  @override
  _NameState createState() => _NameState(student: student, update: update);
}

class _NameState extends State<Register> {
  bool update;
  Data student = Data();
  _NameState({this.student, this.update});
  bool first = false;
  var id = 0;
  var name;
  var gender = 'Male';
  var phone;
  var email;
  var profile =
      'https://i2.wp.com/www.cycat.io/wp/wp-content/uploads/2018/10/Default-user-picture.jpg?w=1170';
  String url = 'http://192.168.178.56:8080/api/';
  File file;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        file = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (student != null && first == false) {
      id = student.id;
      name = student.name;
      phone = student.phone;
      email = student.email;
      gender = student.gender;
      profile = 'http://192.168.178.56:8080/images/' + student.profile;
      first = true;
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
            child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.orange,
            Colors.red,
            Colors.pink,
          ])),
          child: CustomScrollView(
            slivers: [
              SliverAppBar(
                backgroundColor: Colors.transparent,
                expandedHeight: 100,
                title: Container(
                  padding: EdgeInsets.only(top: 20),
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Text('KOSIGN'),
                      Text(
                        'Good Employee Good Company',
                        style: TextStyle(fontSize: 15),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Container(
        height: MediaQuery.of(context).size.height - 220,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Colors.white,
        ),
        child: Column(
          children: [
            Container(
              padding:
                  EdgeInsets.only(top: 20, left: 30, right: 30, bottom: 10),
              child: Text(
                update==false ? 'Register' : 'Update',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.orange[800]),
              ),
            ),
            Text(
              'Register new account with us.',
              style: TextStyle(color: Colors.grey),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              width: 150,
              height: 150,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.circular(200),
                      child: file == null
                          ? Image.network(profile)
                          : Image.file(
                              file,
                              fit: BoxFit.cover,
                              width: 150,
                              height: 150,
                            )),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(300),
                      child: FlatButton(
                          minWidth: 150,
                          height: 150,
                          onPressed: () {
                            getImage();
                          },
                          child: Text(''))),
                  Positioned(
                    top: 115,
                    left: 105,
                    child: CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.grey[600].withOpacity(0.9),
                        child: Icon(
                          Icons.add_a_photo,
                          size: 15,
                          color: Colors.white,
                        )),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: TextFormField(
                initialValue: name,
                onChanged: (val) {
                  setState(() {
                    name = val;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Full name', icon: Icon(Icons.person_outline)),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 10),
              child: TextFormField(
                initialValue: phone != null ? phone.toString() : '',
                onChanged: (val) {
                  setState(() {
                    phone = val;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Phone',
                    icon: Icon(
                      Icons.phone_outlined,
                    )),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 10),
              child: TextFormField(
                initialValue: email,
                onChanged: (val) {
                  setState(() {
                    email = val;
                  });
                },
                decoration: InputDecoration(
                    hintText: 'Email', icon: Icon(Icons.mail_outline)),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 10),
              child: Row(
                children: [
                  Text('Gender : '),
                  Container(
                      width: 100,
                      padding: EdgeInsets.all(0),
                      child: FlatButton.icon(
                          padding: EdgeInsets.all(0),
                          onPressed: () {
                            setState(() {
                              gender = 'Male';
                            });
                          },
                          icon: Radio(
                              value: 'Male',
                              groupValue: gender,
                              onChanged: (val) {
                                setState(() {
                                  gender = val;
                                });
                              }),
                          label: Text('Male'))),
                  Container(
                      width: 130,
                      child: FlatButton.icon(
                          onPressed: () {
                            setState(() {
                              gender = 'Female';
                            });
                          },
                          icon: Radio(
                              value: 'Female',
                              groupValue: gender,
                              onChanged: (val) {
                                setState(() {
                                  gender = val;
                                });
                              }),
                          label: Text('Female'))),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: FlatButton(
                  color: Colors.orange[900],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  padding:
                      EdgeInsets.only(top: 20, bottom: 20, left: 40, right: 40),
                  onPressed: () async {
                    if (update == false) {
                      uploadImage(file.path, 'http://192.168.178.56:8080/upload').then((value) => Navigator.pop(context));
                    } else {
                      updateStudent(file.path, 'http://192.168.178.56:8080/upload').then((value) => Navigator.pop(context));
                    }
                  },
                  child: Text(
                    update==false ? 'Register' : 'Update',
                    style: TextStyle(color: Colors.white),
                  )),
            )
          ],
        )),)
              ),
            ],
          ),
        )),
      ),
    ); 
  }

  Future<String> uploadImage(filename, url) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(await http.MultipartFile.fromPath('image', filename));
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    var result = await http.post( 'http://192.168.178.56:8080/api/student',
      body: jsonEncode({
        "email": email,
        "gender": gender,
        "name": name,
        "phone": phone,
        "profile": convert.jsonDecode(response.body)['fileName']
      }),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
  }

  Future updateStudent(filename, url) async {
    var url1 = 'http://192.168.178.56:8080/api/student/$id';
    var client = http.Client();
    var response = await client.get(url1);
    if (response.statusCode == 200) {
      ResponseMy res = ResponseMy.fromJson(convert.jsonDecode(response.body));
      if (res.message != "Not found") {
        var request = http.MultipartRequest('POST', Uri.parse(url));
        request.files.add(await http.MultipartFile.fromPath('image', filename));
        var res = await request.send();
        var response = await http.Response.fromStream(res);
        await http.put(
          'http://192.168.178.56:8080/api/student',
          body: jsonEncode({
            "id": id,
            "email": email,
            "gender": gender,
            "name": name,
            "phone": phone,
            "profile": convert.jsonDecode(response.body)['fileName']
          }),
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
          },
        );
      } else {
        print("Not found");
      }
    } else {
      print("Erro hz np");
    }
  }
}
