import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class Slide extends StatefulWidget {
  Slide({Key key}) : super(key: key);

  @override
  _SlideState createState() => _SlideState();
}

class _SlideState extends State<Slide> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color.fromRGBO(244, 245, 250, 1),
        body: SafeArea(
            child: Container(
          child: Stack(
            children: [
              Expanded(
                flex: 2,
                child: ExampleVertical()),
             
            ],
          ),
        )),
      ),
    );
  }
}

class ExampleVertical extends StatelessWidget {
  var images = ['assets/images/slide1.png', 'assets/images/slide2.jpeg'];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Swiper(
      itemBuilder: (BuildContext context, int index) {
        return new Image.asset(
          images[index],
          fit: BoxFit.cover,
        );
      },
      autoplay: false,
      itemCount: images.length,
      scrollDirection: Axis.horizontal,
      pagination: new SwiperPagination(alignment: Alignment.bottomCenter),
    ));
  }
}
