import 'package:flutter/material.dart';

class CustomAppBarScreen extends StatelessWidget with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.blueGrey[900],
      title: Text("User Detail"),
      actions: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
          child: InkWell(
            onTap: () {},
            child: Icon(Icons.notifications_active_outlined),
          ),
        ),
      ],
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
