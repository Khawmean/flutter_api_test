import 'package:flutter/material.dart';
import 'package:test_flutter/Student.dart';

// ignore: must_be_immutable
class ViewUserLoginScreen extends StatelessWidget {
  final Data student;
  var screenHeight = 0;

  ViewUserLoginScreen(this.student);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor:
            student.gender == 'Female' ? Colors.pink[700] : Colors.blue[700],
        title: Text("\t\tAbout me"),
        actions: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
            child: Row(
              children: [
                Text("Edit\t\t"),
                InkWell(
                  onTap: () {},
                  child: Icon(
                    Icons.edit_rounded,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      body: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(0.0), color: Colors.blue[100]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.0),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 50.0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.network(
                              'http://192.168.178.56:8080/images/' +
                                  student.profile,
                              fit: BoxFit.cover,
                              height: 70,
                              width: 70,
                            ),
                          ),
                        )
                      ],
                    ),
                    Text(
                      student.name,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: student.gender == 'Female'
                            ? Colors.pink[700]
                            : Colors.blue[700],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: screenHeight * .001),
            Container(
              margin:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 0.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7.0),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.check_circle,
                          size: 18,
                        ),
                        Text(
                          "\tSummary",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Colors.black87),
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 30,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.grey[300]),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7.0, 0, 0, 0),
                            child: Icon(
                              Icons.account_circle,
                              size: 20,
                            ),
                          ),
                          Text("\t" + student.name),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 30,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.grey[300]),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7.0, 0, 0, 0),
                            child: Icon(
                              Icons.grade,
                              size: 20,
                            ),
                          ),
                          Text("\t" + student.gender),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 30,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.grey[300]),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7.0, 0, 0, 0),
                            child: Icon(
                              Icons.email_outlined,
                              size: 20,
                            ),
                          ),
                          Text("\t" + student.email),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 30,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.grey[300]),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7.0, 0, 0, 0),
                            child: Icon(
                              Icons.call,
                              size: 20,
                            ),
                          ),
                          Text(
                            student.phone.toString(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 30,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 5.0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: Colors.grey[300]),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7.0, 0, 0, 0),
                            child: Icon(
                              Icons.date_range,
                              size: 20,
                            ),
                          ),
                          Text("\t 28 Jun 2021"),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: screenHeight * .02,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    print("Changed");
                  },
                  child: Container(
                    width: screenHeight * .165,
                    alignment: Alignment.centerRight,
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 0.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Change language",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.black87),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
