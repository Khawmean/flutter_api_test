import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:test_flutter/detail.dart';
import 'package:test_flutter/register.dart';
import 'package:test_flutter/screens/view_user_login_screen.dart';
import 'package:test_flutter/testApp.dart';

import 'Student.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text('API'),
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              child: App(),
            ),
          ),
        ),
      ),
    );
  }
}

class App extends StatefulWidget {
  App({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NameState();
}

class _NameState extends State<App> {
  int ab = 0;
  bool noData  ;
  Future<Student> student;

  void deleteApi(id) {
    http.delete(
      'http://192.168.178.56:8080/api/student/$id',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    student = getData();
  
    return FutureBuilder(
            future: student,
            builder: (context, snapsort) {
              return snapsort.data != null ? Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: FlatButton.icon(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Register(
                                          update: false,
                                        )),
                              ).then(onGoBack);
                            },
                            color: Colors.pink,
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Colors.white,
                            ),
                            label: Text(
                              'Register',
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                      Padding(
                          padding: EdgeInsets.all(20),
                          child: Text(snapsort.data.message)),
                      Container(
                        height: 700,
                        child: ListView.builder(
                            itemCount: snapsort.data.data.length,
                            itemBuilder: (context, index) {
                              return Dismissible(
                                key: Key(
                                    snapsort.data.data[index].id.toString()),
                                onDismissed: (direction) {
                                  if (direction ==
                                      DismissDirection.endToStart) {
                                    deleteApi(snapsort.data.data[index].id);
                                  } else {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Register(
                                                student:
                                                    snapsort.data.data[index],
                                                update: true,
                                              )),
                                    ).then(onGoBack);
                                  }
                                },
                                background: Container(
                                  color: Colors.blue[100],
                                  child: FlatButton.icon(
                                    icon: Icon(Icons.edit),
                                    label: Text('Update'),
                                  ),
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(left: 10),
                                ),
                                secondaryBackground: Container(
                                  color: Colors.red[100],
                                  child: FlatButton.icon(
                                    icon: Text('Delete'),
                                    label: Icon(Icons.delete),
                                  ),
                                  alignment: Alignment.centerRight,
                                  padding: EdgeInsets.only(right: 10),
                                ),
                                child: ListTile(
                                  onTap: () {
                                    // http.get.then(val){ naviator.push}
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Detail(id: snapsort.data.data[index].name,)));
                                  },
                                  leading: CircleAvatar(
                                    child: snapsort.data.data[index].profile !=
                                            null
                                        ? ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            child: Image.network(
                                              'http://192.168.178.56:8080/images/' +
                                                  snapsort
                                                      .data.data[index].profile,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        : Icon(Icons.person),
                                  ),
                                  title: Text(snapsort.data.data[index].name),
                                  subtitle:
                                      Text(snapsort.data.data[index].email),
                                  trailing: Text('0' +
                                      snapsort.data.data[index].phone
                                          .toString()),
                                ),
                              );
                            }),
                      ),
                    ],
                  )) : Text('No data');
            });
      
  }

  onGoBack(dynamic value) {
    ab++;
    setState(() {
      student = getData();
    });
  }
}

Future<Student> getData() async {
    var url = 'http://192.168.178.56:8080/api/';
    var client = http.Client();
    try {
      var response = await client.get(url + 'students');
      if (response.statusCode == 200) {
        Student res = Student.fromJson(convert.jsonDecode(response.body));
        return res;
      } else {
        return null;
      }
    } catch (e) {
      print("Can not retriev data!");
      return null;
    }
  }
